/* library voor de RGB leds*/
#include <FastLED.h>

/* constanten / instellingen voor de leds */
#define RGB_LED_BRIGHTNESS  10 // 0-255
#define RGB_LED_DATA_PIN    13  // D7
#define RGB_LED_CLOCK_PIN   14  // D5
#define RGB_NUM_LEDS        2
#define RGB_LED_TYPE        APA102
#define RGB_COLOR_ORDER     BGR

uint8_t gHue = 0; // rotating "base color" used by many of the patterns

/* variabelen */
CRGB leds[RGB_NUM_LEDS];
short color_index = 0;
CRGB colors[] = {CRGB::Red, CRGB::Green, CRGB::Blue, CRGB::Black};
/* voor meer kleuren zie: http://fastled.io/docs/3.1/group___pixeltypes.html#gaeb40a08b7cb90c1e21bd408261558b99 
black is uit */

/*
   RGB leds
*/
void setRgbLeds(CRGB color) {
  for (int i = 0; i < RGB_NUM_LEDS; i++) {
    setRgbLed(i, color);
  }
}

void setRgbLed(int ledno, CRGB color) {
  leds[ledno] = color;
  FastLED.show();
}

void setup() {
  // put your setup code here, to run once:
  // Initialiseer RGB leds
  FastLED.addLeds<RGB_LED_TYPE, RGB_LED_DATA_PIN, RGB_LED_CLOCK_PIN, RGB_COLOR_ORDER>(leds, RGB_NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(RGB_LED_BRIGHTNESS);
  setRgbLeds(CRGB::Black); // beide leds uit 
}

void loop() {
  
  // FastLED's built-in rainbow generator
  fill_rainbow( leds, RGB_NUM_LEDS, gHue, 7);
  // send the 'leds' array out to the actual LED strip
  FastLED.show();  
  // insert a delay to keep the framerate modest
  FastLED.delay(20); 

  // do some periodic updates
  EVERY_N_MILLISECONDS( 4 ) { gHue++; } // slowly cycle the "base color" through the rainbow
}
