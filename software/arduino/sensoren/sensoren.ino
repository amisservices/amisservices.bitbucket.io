/* libraries voor de temperatuur sensor */
#include <Wire.h>
#include <LM75.h>

/* Runtime variabelen */
LM75 temp_sensor;

void setup() {
  // put your setup code here, to run once:

  // Start I2C voor temperatuursensor
  Wire.begin();

  //voor seriele monitor
  Serial.begin(115200);
  delay(10);
  Serial.println();
}

/* Lichtsensor */
float getLuminance() {
  // Meet voltage op analoge input en reken dit om in het equivalente aantal lux
  float volts = analogRead(A0) * 3.3 / 1024.0;
  float amps = volts / 10000.0;  // 10k weerstand
  float microamps = amps * 1000000;
  float lux = microamps * 2.0;

  return lux;
}

/* Temperatuursensor */
float getTemperature() {
  // Sensor activeren
  temp_sensor.shutdown(false);
  delay(100);

  float temperature = temp_sensor.temp();

  // Sensor deactiveren
  temp_sensor.shutdown(true);

  return temperature;
}

/* Lees sensoren uit */
void readSensors() {
  Serial.print("Temperatuur: ");
  Serial.print(getTemperature());
  Serial.println("°C");

  Serial.print("Hoeveelheid licht: ");
  Serial.print(getLuminance());
  Serial.println(" lux");
}

void loop() {
  // put your main code here, to run repeatedly:

  readSensors();

  delay(2000);
}
