/* library voor de RGB leds*/
#include <FastLED.h>

/* constanten / instellingen voor de leds */
#define RGB_LED_BRIGHTNESS  10 // 0-255
#define RGB_LED_DATA_PIN    13  // D7
#define RGB_LED_CLOCK_PIN   14  // D5
#define RGB_NUM_LEDS        2
#define RGB_LED_TYPE        APA102
#define RGB_COLOR_ORDER     BGR

/* variabelen */
CRGB leds[RGB_NUM_LEDS];
short color_index = 0;
CRGB colors[] = {CRGB::Red, CRGB::Green, CRGB::Blue, CRGB::Black};
/* voor meer kleuren zie: http://fastled.io/docs/3.1/group___pixeltypes.html#gaeb40a08b7cb90c1e21bd408261558b99 
black is uit */

/*
   RGB leds
*/
void setRgbLeds(CRGB color) {
  for (int i = 0; i < RGB_NUM_LEDS; i++) {
    setRgbLed(i, color);
  }
}

void setRgbLed(int ledno, CRGB color) {
  leds[ledno] = color;
  FastLED.show();
}

void setup() {
  // put your setup code here, to run once:
  // Initialiseer RGB leds
  FastLED.addLeds<RGB_LED_TYPE, RGB_LED_DATA_PIN, RGB_LED_CLOCK_PIN, RGB_COLOR_ORDER>(leds, RGB_NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(RGB_LED_BRIGHTNESS);
  setRgbLeds(CRGB::Black); // beide leds uit 
}

void loop() {
  // put your main code here, to run repeatedly:
  EVERY_N_SECONDS(1) {
    setRgbLeds(colors[color_index]);
    color_index++;
    if(color_index>=(sizeof(colors)/sizeof(CRGB)) {
      color_index=0;
    }
  }
}
