const char* ssid = "iot-workshop";        
const char* password = "123456ab";  

char* mqtt_node_id = "ESP_hostname";
const char* mqtt_server = "mqtt.t11r.nl";

float lastTemperature = 0;
float lastLux = 0;
int buttonClicks = 0;

void setupGeneral() {
  Serial.begin(115200);
  Serial.println();
  Serial.println();
}

void setup() {
  setupGeneral();
  setupWifi();
  setupMqtt();
  setupTemperature();
  setupLight();
  setupButton();
}

void loop() {
  loopMqtt();
  loopTemperature();
  loopLight();
  loopButton();
}
