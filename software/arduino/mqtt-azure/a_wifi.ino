#include <ESP8266WiFi.h>

WiFiClient espClient;

void setupWifi() {
  WiFi.mode(WIFI_STA);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println();
  Serial.print("IP: ");
  Serial.println(WiFi.localIP());
  Serial.print("Hostname: ");
  Serial.println(WiFi.hostname());
  WiFi.hostname().toCharArray(mqtt_node_id,20);
}

