const char* ssid = "AndroidAP";        
const char* password = "xgfv9523";  

char* mqtt_node_id = "mqttdash-utx1gR5kNW";
const char* mqtt_server = "192.168.43.223";

float lastTemperature = 0;
float lastLux = 0;
int buttonClicks = 0;

void callback(char* topic, byte* payload, unsigned int length); // callback header

void setupGeneral() {
  Serial.begin(115200);
  Serial.println();
  Serial.println();
}

void setup() {
  setupGeneral();
  setupWifi();
  setupMqtt();
  setupTemperature();
  setupLight();
  setupButton();
  setupLED();
}

void loop() {
  loopMqtt();
  loopTemperature();
  loopLight();
  loopButton();
}
