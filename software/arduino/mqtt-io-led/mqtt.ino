#include <PubSubClient.h>

PubSubClient client(espClient);
char base_topic[100];
char will_topic[200];
int mqtt_lastUpdate = 0;
int mqtt_update_interval = 3000; // 3 seconden

void mqtt_reconnect() {
  while (!client.connected()) {
    Serial.println("mqtt reconnect");

    if (client.connect(mqtt_node_id, will_topic, 2, true, "OFF")) {
      Serial.println("mqtt connected");
      client.publish(will_topic, "ON");
    } else {
      delay(1000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) { // callback registers incoming messages

  Serial.println("Message arrived:  topic: " + String(topic));
  Serial.println("Length: " + String(length,DEC));

  char message_buffer[100];
  
  // create character buffer with ending null terminator (string)
  // to later convert to String
  // this is because payload is unsigned char and cannot directly be compared to constant chars
  int i = 0;
  for(i=0; i<length; i++) {
    message_buffer[i] = payload[i];
  }
  message_buffer[i] = '\0';
  
  String msgString = String(message_buffer);
  
  Serial.println("Payload: " + msgString);
  if (msgString == "1"){
    digitalWrite(LED_BUILTIN, LOW);
    pub("output","LED ON"); 
  } else if (msgString == "0"){
    digitalWrite(LED_BUILTIN, HIGH);
    pub("output","LED OFF"); 
  }
}

void pubInt(char *topic, int value) {
  char b[10];
  sprintf(b, "%d", value);
  pub(topic, b);
}

char* floatToChar(float value) {
  char b[10];
  dtostrf(value, 1, 2, b);
  return b;
}

void pubFloat(char *topic, float value) {
  pub(topic, floatToChar(value));
}

void pub(char *topic, char *value) {
  char full_topic[200];
  sprintf(full_topic, "%s/%s", base_topic, topic);
  client.publish(full_topic, value);
}

void sub(char *topic) {
  char full_topic[200];
  sprintf(full_topic, "%s/%s", base_topic, topic);
  client.subscribe(full_topic);
}

void setupMqtt() {
  client.setServer(mqtt_server, 80);
  client.setCallback(callback);

  sprintf(base_topic, "azure/%s", mqtt_node_id);
  sprintf(will_topic, "%s/%s", "state", mqtt_node_id);
}

void loopMqtt() {
  if (!client.connected()) {
    mqtt_reconnect();
  } else {
    client.loop();
  }
  if (millis() > mqtt_update_interval && 
       ((millis() - mqtt_update_interval) > mqtt_lastUpdate)) {
    mqtt_lastUpdate = millis();
  
    char json[200];
    sprintf(json, "{ \"id\": \"%s\", \"tmp\": %s,  \"lum\": %s,  \"btn\": %d}", mqtt_node_id, String(lastTemperature).c_str(), String(lastLux).c_str(), buttonClicks); 
    pub("output",json);
    Serial.println(json);
    sub("LED");
  }
}

String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length() - 1;
  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

